import React, { useState, useEffect } from 'react';
import './App.css';
import BookList from './components/BookList';
import Form from './components/Form';
import Alert from './components/Alert';


const App = () => {
  const [books, setBooks] = useState([
    {
      title: 'To Kill a Mockingbird',
      author: 'Harper Lee',
      pages: 290,
      publisher: 'J.B. Lippincott Company',
      pubYear: 1966,
      id: 0
    },
    {
      title: 'To Kill a Mockingbird2',
      author: 'Harper Lee',
      pages: 290,
      publisher: 'J.B. Lippincott Company',
      pubYear: 1957,
      id: 1
    },
    {
      title: 'To Kill a Mockingbird3',
      author: 'Harper Lee',
      pages: 290,
      publisher: 'J.B. Lippincott Company',
      pubYear: 1959,
      id: 2
    }
  ]);

  /*useEffect(() => (
    console.log('books render')
  ), [books]);
  useEffect(() => (
    fetch('https://api.npoint.io/4a16d9d0ec37532797f9')
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        console.log(data);
      })
  ), []);*/


  const [filter, setFilter] = useState(localStorage.getItem('filter') ? localStorage.getItem('filter') : '');

  useEffect(() => (
    sortBooks()
  ), [filter]);


  const [alertText, setAlertText] = useState('')


  function deleteBook(id) {
    const remainingBooks = books.filter(book => id !== book.id);
    setBooks(remainingBooks);
    //localStorage.removeItem(id.toString());
  };

  function editBook(id, replacementData) {
    const editedBook = books.map((book) => {
      if (id !== book.id) {
        return { ...book };
      }
      else {
        return replacementData;
      }
    });
    setBooks(editedBook);
  }
  function sortBooks(){
      let arr = [books][0];
      arr.sort((a, b) => {
        return a[filter] > b[filter] ? 1 : -1});
  }

    sortBooks();

  return <div className="mainApp">
    <div className="container">
      <Alert alertText={alertText} setAlertText={setAlertText} />
      <Form books={books} setBooks={setBooks} filter={filter} setFilter={setFilter} setAlertText={setAlertText} />
      <BookList books={books} editBook={editBook} deleteBook={deleteBook} setBooks={setBooks} />
    </div>
  </div>
}

export default App;

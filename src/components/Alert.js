import React from 'react';
import './Alert.css'

export default function Alert({ alertText, setAlertText }) {

    

    return alertText ? (
        <div className='alert-wrapper'>
        <div className='alert'>
            <span className='alert__text'>
                {alertText}
            </span>
            <button className='alert__btn' onClick={ () => {setAlertText('')}}>&times;</button>
        </div>
        </div>
    )
        : null;
};

import React, { useState, useEffect } from 'react';
import plus from '../assets/plus.svg';
import './Form.css';
import NewItem from './NewItem';

const Form = ({ books, setBooks, filter, setFilter, setAlertText }) => {

    const [newItem, setNewItem] = useState({
        title: '',
        author: '',
        pages: '',
        publisher: '',
        pubYear: '',
        id: localStorage.length
    });
    const [showForm, setShowFrom] = useState(false);
    const [verified, setVerified] = useState(false);


    const changeCurrentFilter = (e) => {
        let filterParametr = e.target.value;
        /*const remainingBooks = books.sort(function (a, b) {
            return parseInt(a[filterParametr]) - parseInt(b[filterParametr]);
        });
        console.log(typeof remainingBooks, remainingBooks)
        setBooks(remainingBooks);*/
        setFilter(filterParametr);
        localStorage.setItem('filter', filterParametr);
    };

    const formInputUpdate = (e) => {
        let element = e.target;
        switch (element.name) {
            case 'title':
                if (element.value.length <= 30 && element.value.length > 0) {
                    element.nextElementSibling.style.display = 'none';
                    setVerified(true);
                }
                else {
                    element.nextElementSibling.style.display = 'flex';
                    setVerified(false);
                }
                break;
            case 'author':
                if (element.value.length !== 0) {
                    element.nextElementSibling.style.display = 'none';
                    setVerified(true);
                }
                else {
                    element.nextElementSibling.style.display = 'flex';
                    setVerified(false);
                }
                break;
            case 'pages':
                if (parseInt(element.value) > 0 && parseInt(element.value) < 10000) {
                    element.nextElementSibling.style.display = 'none';
                    setVerified(true);
                }
                else {
                    element.nextElementSibling.style.display = 'flex';
                    setVerified(false);
                }
                break;
            case 'publisher':
                if (element.value.length <= 30 && element.value.length > 0) {
                    element.nextElementSibling.style.display = 'none';
                    setVerified(true);
                }
                else {
                    element.nextElementSibling.style.display = 'flex';
                    setVerified(false);
                }
                break;
            case 'pubYear':
                if (element.value >= 1800) {
                    element.nextElementSibling.style.display = 'none';
                    setVerified(true);
                }
                else {
                    element.nextElementSibling.style.display = 'flex';
                    setVerified(false);
                }
                break;

            default:
                setNewItem({ ...newItem, [element.name]: element.value });
                break;
        }
        setNewItem({ ...newItem, [element.name]: element.value });
    };

    const resetForm = () => {
        setNewItem({
            title: '',
            author: '',
            pages: '',
            publisher: '',
            pubYear: '',
            id: Math.random() * 1000
        });
    };

    const submitNewItem = (e) => {
        console.log('varified ', verified)
        e.preventDefault();
        if (verified) {
            //localStorage.setItem(newItem.id.toString(), JSON.stringify(newItem));
            if (newItem.title) {
                setBooks([
                    ...books, newItem
                ]);
            }
            resetForm();
            toggleForm();
            setAlertText('');
        }
        else {
            setAlertText('Please, fill all fileds!')
        }
    };
    const cancelNewItem = () => {
        setShowFrom(false);
        resetForm();
        setAlertText('');
    };
    const toggleForm = () => {
        setShowFrom(showing => !showing);
    };

    return (

        <div className='form'>
            <select onChange={changeCurrentFilter} value={filter}>
                <option defaultValue></option>
                <option value='pubYear'>Year</option>
                <option value='title'>Title</option>
            </select>
            <div className='toggleForm' onClick={toggleForm}>
                <img src={plus} alt='plus' />
            </div>
            {
                showForm ?
                    <NewItem item={newItem} setItem={setNewItem} formInputUpdate={formInputUpdate} submitNewItem={submitNewItem} cancelNewItem={cancelNewItem} />
                    : null
            }
        </div>
    );
};

export default Form;
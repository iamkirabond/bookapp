import React from 'react';
import plus from '../assets/plus.svg';


const NewItem = ({ item, setItem, formInputUpdate, submitNewItem, cancelNewItem }) => {

    const addAnotherAuthor = () => {
        setItem(prevProp => {
            return prevProp.author = prevProp.author.push('');
        })
    };

    return (
        <div className='newItemWrapper'>
            <form className='newItem booklist__item edit booklist__field'>
                <div className="booklist__field booklist__title">
                    <label>Title:</label>
                    <input onChange={formInputUpdate} name='title' value={item.title} placeholder='Title' required></input>
                    <span className='booklist__warning booklist__warning_title'>Please, enter shorter title</span>
                </div>
                <div className="booklist__field booklist__author">
                    <label>Authors:</label>
                    <div className='booklist__author_inputs'>
                        <input onChange={formInputUpdate} name='author' value={item.author} placeholder='Authors' required></input>
                    </div>
                    <span className='booklist__warning booklist__warning_title'>Please, enter the author name</span>
                </div>
                <div className="booklist__field booklist__pages">
                    <label>Pages:</label>
                    <input onChange={formInputUpdate} name='pages' value={item.pages} placeholder='Pages' required></input>
                    <span className='booklist__warning booklist__warning_title'>Type any number less than 10 000</span>
                </div>
                <div className="booklist__field booklist__publisher">
                    <label>Publisher:</label>
                    <input onChange={formInputUpdate} name='publisher' value={item.publisher} placeholder='Publisher' required></input>
                    <span className='booklist__warning booklist__warning_title'>Keep publisher name simple</span>
                </div>
                <div className="booklist__field booklist__pubyear">
                    <label>Year:</label>
                    <input onChange={formInputUpdate} name='pubYear' value={item.pubYear} placeholder='Year' required></input>
                    <span className='booklist__warning booklist__warning_title'>Any year since 1800</span>
                </div>
                <div className='newItem__btns booklist__btns'>
                    <button onClick={submitNewItem}>Submit</button>
                    <button onClick={cancelNewItem}>Cancel</button>
                </div>
            </form>
        </div>
    )
};

export default NewItem;
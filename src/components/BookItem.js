import React, { useState } from 'react';
import './BookItem.css';

const BookItem = ({ book, editBook, deleteBook }) => {


    const [isEditing, setIsEditing] = useState(false);
    const [replacement, setReplacement] = useState({ ...book });

    const formInputUpdate = (e) => {
        let name = e.target.name;
        let value = e.target.value;
        setReplacement({ ...replacement, [name]: value });
    };

    const toggleEdit = () => {
        setIsEditing(!isEditing);
    };

    const replaceBookData = () => {
        editBook(book.id, replacement);
        toggleEdit();
    }

    const viewTemplate = (book) => {
        return (<li className="booklist__item" key={book.id}>
            <div className="booklist__info">
                <h2 className='booklist__title'>{book.title}</h2>
                <h3 className='booklist__author'>Author: {book.author}</h3>
                <span className='booklist__pages'>Pages: {book.pages}</span>
                <span className='booklist__publisher'>Publisher: {book.publisher}</span>
                <span className='booklist__pubyear'>Year: {book.pubYear}</span>
            </div>
            <div className="booklist__btns">
                <button type="button" onClick={() => toggleEdit(book.id)}>Edit</button>
                <button type="button" onClick={() => deleteBook(book.id)}>Delete</button>
            </div>
        </li>)

    };
    const editingTemplate = (book) => {
        return (<form className="booklist__item edit">
            <div className="booklist__info">
                <div className="booklist__field booklist__title">
                    <label>Title:</label>
                    <input placeholder={book.title} onChange={formInputUpdate} name='title' value={replacement.title}></input>
                </div>
                <div className="booklist__field booklist__author">
                    <label>Author:</label>
                    <input placeholder={book.author} onChange={formInputUpdate} name='author' value={replacement.author}></input>
                </div>
                <div className="booklist__field booklist__pages">
                    <label>Pages:</label>
                    <input placeholder={book.pages} onChange={formInputUpdate} name='pages' value={replacement.pages}></input>
                </div>
                <div className="booklist__field booklist__publisher">
                    <label>Publisher:</label>
                    <input placeholder={book.publisher} onChange={formInputUpdate} name='publisher' value={replacement.publisher}></input></div>
                <div className="booklist__field booklist__pubyear">
                    <label>Year:</label>
                    <input placeholder={book.pubYear} onChange={formInputUpdate} name='pubYear' value={replacement.pubYear}></input>
                </div>
            </div>
            <div className="booklist__btns">
                <button type="button" onClick={replaceBookData}>Save</button>
                <button type="button" onClick={toggleEdit}>Cancel</button>
            </div>
        </form >)
    };
    return (
        isEditing ? editingTemplate(book) : viewTemplate(book)
    );
};

export default BookItem;

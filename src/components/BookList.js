import React from 'react';
import './BookList.css';
import BookItem from './BookItem';

const BookList = ({ books, editBook, deleteBook }) => {

    return (
        <ul className="booklist">
            {books.map((book) =>
                <BookItem book={book} editBook={editBook} deleteBook={deleteBook} key={book.id}/>
            )}
        </ul>
    );
}

export default BookList;
